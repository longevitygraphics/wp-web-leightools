<?php
$image_id = get_post_meta( get_the_ID(), '_listing_image_id', true );
$img      = wp_get_attachment_url( $image_id );
if ( $img ) : ?>
	<img class="img-full product-banner" src="<?php echo $img ?>"/>
<?php endif; ?>


<div id="product-tabs" class="product-tabs product-tabs--sticky">

		<!--tabs appended here-->

</div>
