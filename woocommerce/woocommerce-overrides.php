<?php

//remove related products from product page
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

//remove default woocommerce tabs from product page
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );


//add products tabs content
add_action( 'woocommerce_after_single_product', 'lg_add_product_tabs', 5 );
function lg_add_product_tabs() {
	wc_get_template_part( 'product', 'tabs-content' );
}


//add product banner image from metabox
add_action( 'woocommerce_before_single_product', 'lg_add_product_banner' );
function lg_add_product_banner() {
	wc_get_template_part( 'product', 'banner-and-tabs' );
}


//move the product meta after product title
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 7 );


//move the product short description after SKU
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 8 );


//remove price. It is added in woocommerce/single-product/add-to-cart/variation-add-to-cart-button.php next to quantity
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

/*remove_action( 'woocommerce_single_product_summary',
	'woocommerce_template_single_add_to_cart', 30 );
add_action( 'woocommerce_single_product_summary',
	'woocommerce_template_single_add_to_cart', 5 );*/

/*function lg_clearfix(){
	echo '<div class="clearfix"></div>';
}
add_action('woocommerce_after_single_product', 'lg_clearfix', 5);*/


