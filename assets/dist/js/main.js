/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/entry.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/entry.js":
/*!*************************!*\
  !*** ./assets/entry.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./src/js/script */ "./assets/src/js/script.js");

/***/ }),

/***/ "./assets/src/js/script.js":
/*!*********************************!*\
  !*** ./assets/src/js/script.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sass_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../sass/style.scss */ "./assets/src/sass/style.scss");
/* harmony import */ var _sass_style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_sass_style_scss__WEBPACK_IMPORTED_MODULE_0__);


__webpack_require__(/*! ./window-event/ready.js */ "./assets/src/js/window-event/ready.js");

__webpack_require__(/*! ./window-event/load.js */ "./assets/src/js/window-event/load.js");

__webpack_require__(/*! ./window-event/resize.js */ "./assets/src/js/window-event/resize.js");

__webpack_require__(/*! ./window-event/scroll.js */ "./assets/src/js/window-event/scroll.js");

/***/ }),

/***/ "./assets/src/js/window-event/load.js":
/*!********************************************!*\
  !*** ./assets/src/js/window-event/load.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Windows Load Handler
(function ($) {
  $(window).on('load', function () {});
})(jQuery);

/***/ }),

/***/ "./assets/src/js/window-event/ready.js":
/*!*********************************************!*\
  !*** ./assets/src/js/window-event/ready.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Windows Ready Handler
(function ($) {
  $(document).ready(function () {
    // ------------------------------------------------------- //
    // Multi Level dropdowns
    // ------------------------------------------------------ //
    $("ul.dropdown-menu [data-toggle='dropdown']").on("click", function (event) {
      event.preventDefault();
      event.stopPropagation();
      $(this).attr('aria-expanded', function (i, attr) {
        return attr === 'true' ? 'false' : 'true';
      });
      $(this).siblings().toggleClass("show"); //$(this).parents('.dropdown-submenu').toggleClass('open');

      /* if (!$(this).next().hasClass('show')) {
         $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
       }*/

      /* $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
         $('.dropdown-submenu .show').removeClass("show");
       });*/
    });
    $('.footer-menu').children('.toggle-button').click(function () {
      $(this).parent('.footer-menu').toggleClass('show-links');
      $(this).next('section').slideToggle();
    }); //currency switcher

    $(".country-switcher-menu a").click(function () {
      var $this = $(this);
      var select = $('#country-switcher');
      var selectedCurrency = $this.attr('data-value');
      var currentCurrency = select.val();

      if (currentCurrency !== selectedCurrency) {
        select.val(selectedCurrency).trigger('change');
      }

      return false;
    }); //product tabs appended after product banner

    var stickyNavTabs = $(".wp-block-lg-blocks-tabs .nav-tabs");

    if (stickyNavTabs.length) {
      stickyNavTabs.appendTo($("#product-tabs"));
      $('#product-tabs a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $activeTarget = e.target;
        var $prevTarget = e.relatedTarget;
        var overviewTabIsActive = $($activeTarget).attr('id') === 'lgTab-Overview';
        var productInfoDiv = $(".product");

        if (overviewTabIsActive) {
          productInfoDiv.fadeIn();
        } else {
          productInfoDiv.fadeOut(200);
        }
      });
    }
    /**
     * covert the default dropdown to tiles
     */


    $('.variations select').each(function () {
      $(this).togglebutton();
    });
  }); // Define the togglebutton plugin.

  $.fn.togglebutton = function (opts) {
    // Apply the users options if exists.
    var settings = $.extend({}, $.fn.togglebutton.defaults, opts); // For each select element.

    this.each(function () {
      var self = $(this);
      var multiple = this.multiple; // Retrieve all options.

      var options = self.children('option'); // Create an array of buttons with the value of select options.

      var buttons = options.map(function (index, opt) {
        var button = $("<button type='button' class='btn btn-default'></button>").prop('value', opt.value).text(opt.text); // Add an `active` class if the option has been selected.

        if (opt.selected) button.addClass("active"); // Return the button.

        return button[0];
      }); // For each button, implement the click button removing and adding
      // `active` class to simulate the toggle effect. And also change the
      // select selected option.

      buttons.each(function (index, btn) {
        $(btn).click(function () {
          // Retrieve all buttons siblings of the clicked one with an
          // `active` class !
          var activeBtn = $(btn).siblings(".active");
          var total = []; // Check if the clicked button has the class `active`.
          // Add or remove it according to the check.

          if ($(btn).hasClass("active")) {
            //$(btn).removeClass("active");
            return true;
          } else {
            $(btn).addClass("active");
            options.val(btn.value).prop("selected", true);
            total.push(btn.value);
          } // Remove all selected property on options.


          self.children("option:selected").prop("selected", false); // If the select allow multiple values, remove all active
          // class to the other buttons (to keep only the last clicked
          // button).

          if (!multiple) {
            activeBtn.removeClass("active");
          } // Push all active buttons value in an array.


          activeBtn.each(function (index, btn) {
            total.push(btn.value);
          }); // Change selected options of the select.

          self.val(btn.value).change();
        });
      }); // Group all the buttons in a `div` element.

      var btnGroup = $("<div class='btn-group'>").append(buttons); // Include the buttons group after the select element.

      self.after(btnGroup); // Hide the display element.

      self.hide();
    });
  }; // Set the defaults options of the plugin.


  $.fn.togglebutton.defaults = {};
})(jQuery);

/***/ }),

/***/ "./assets/src/js/window-event/resize.js":
/*!**********************************************!*\
  !*** ./assets/src/js/window-event/resize.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Window Resize Handler
(function ($) {
  $(window).on('resize', function () {});
})(jQuery);

/***/ }),

/***/ "./assets/src/js/window-event/scroll.js":
/*!**********************************************!*\
  !*** ./assets/src/js/window-event/scroll.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Window Scroll Handler
(function ($) {
  $(window).on('scroll', function () {});
})(jQuery);

/***/ }),

/***/ "./assets/src/sass/style.scss":
/*!************************************!*\
  !*** ./assets/src/sass/style.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=main.js.map