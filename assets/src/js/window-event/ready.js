// Windows Ready Handler

(function ($) {

  $(document).ready(function () {

    // ------------------------------------------------------- //
    // Multi Level dropdowns
    // ------------------------------------------------------ //
    $("ul.dropdown-menu [data-toggle='dropdown']").on("click", function (event) {
      event.preventDefault();
      event.stopPropagation();
      $(this).attr('aria-expanded', function (i, attr) {
        return attr === 'true' ? 'false' : 'true'
      });

      $(this).siblings().toggleClass("show");

      //$(this).parents('.dropdown-submenu').toggleClass('open');

      /* if (!$(this).next().hasClass('show')) {
         $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
       }*/
      /* $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
         $('.dropdown-submenu .show').removeClass("show");
       });*/

    });

    $('.footer-menu').children('.toggle-button').click(function () {
      $(this).parent('.footer-menu').toggleClass('show-links');
      $(this).next('section').slideToggle();
    });


    //currency switcher
    $(".country-switcher-menu a").click(function () {
      const $this = $(this);
      const select = $('#country-switcher');
      const selectedCurrency = $this.attr('data-value');
      const currentCurrency = select.val();
      if (currentCurrency !== selectedCurrency) {
        select.val(selectedCurrency).trigger('change');
      }
      return false;
    });


    //product tabs appended after product banner
    var stickyNavTabs = $(".wp-block-lg-blocks-tabs .nav-tabs");
    if (stickyNavTabs.length) {
      stickyNavTabs.appendTo($("#product-tabs"));
      $('#product-tabs a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        let $activeTarget = e.target;
        let $prevTarget = e.relatedTarget;
        let overviewTabIsActive = ($($activeTarget).attr('id') === 'lgTab-Overview');
        let productInfoDiv = $(".product");
        if (overviewTabIsActive) {
          productInfoDiv.fadeIn();
        } else {
          productInfoDiv.fadeOut(200);
        }
      })
    }


    /**
     * covert the default dropdown to tiles
     */
    $('.variations select').each(function () {
      $(this).togglebutton();
    });


  });
  // Define the togglebutton plugin.
  $.fn.togglebutton = function (opts) {
    // Apply the users options if exists.
    var settings = $.extend({}, $.fn.togglebutton.defaults, opts);

    // For each select element.
    this.each(function () {
      var self = $(this);
      var multiple = this.multiple;

      // Retrieve all options.
      var options = self.children('option');
      // Create an array of buttons with the value of select options.
      var buttons = options.map(function (index, opt) {
        var button = $("<button type='button' class='btn btn-default'></button>")
          .prop('value', opt.value)
          .text(opt.text);

        // Add an `active` class if the option has been selected.
        if (opt.selected)
          button.addClass("active");

        // Return the button.
        return button[0];
      });

      // For each button, implement the click button removing and adding
      // `active` class to simulate the toggle effect. And also change the
      // select selected option.
      buttons.each(function (index, btn) {
        $(btn).click(function () {
          // Retrieve all buttons siblings of the clicked one with an
          // `active` class !
          var activeBtn = $(btn).siblings(".active");
          var total = [];


          // Check if the clicked button has the class `active`.
          // Add or remove it according to the check.
          if ($(btn).hasClass("active")) {
            //$(btn).removeClass("active");
            return true;
          } else {
            $(btn).addClass("active");
            options.val(btn.value).prop("selected", true);
            total.push(btn.value);
          }

          // Remove all selected property on options.
          self.children("option:selected").prop("selected", false);

          // If the select allow multiple values, remove all active
          // class to the other buttons (to keep only the last clicked
          // button).
          if (!multiple) {
            activeBtn.removeClass("active");
          }

          // Push all active buttons value in an array.
          activeBtn.each(function (index, btn) {
            total.push(btn.value);
          });

          // Change selected options of the select.
          self.val(btn.value).change();
        });
      });

      // Group all the buttons in a `div` element.
      var btnGroup = $("<div class='btn-group'>").append(buttons);
      // Include the buttons group after the select element.
      self.after(btnGroup);
      // Hide the display element.
      self.hide();
    });
  };

  // Set the defaults options of the plugin.
  $.fn.togglebutton.defaults = {};
}(jQuery));
