<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

<?php
$banner        = get_option( 'lg_option_blog_archive_banner_image' );
//$banner_height = get_option( 'lg_option_blog_archive_banner_height' ) ? get_option( 'lg_option_blog_archive_banner_height' ) : '400px';
//$blog_style    = get_option( 'lg_option_blog_style' ) ? get_option( 'lg_option_blog_style' ) : 'list';
?>
<main>
	<?php if ( $banner ): ?>
		<div class="wp-block-cover alignfull page-header"
		     style="background-image:url(<?php echo $banner ?>)">
			<div class="wp-block-cover__inner-container">
				<h1 class="has-dark-color has-text-color"><?php echo get_the_title() ?></h1>
			</div>
		</div>
	<?php endif; ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<div class="">
			<?php the_content(); ?>
		</div>

	<?php endwhile; endif; ?>
</main>
<?php get_footer(); ?>
