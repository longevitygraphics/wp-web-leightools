<?php
$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
if ( $myaccount_page_id ) {
	$myaccount_page_url = get_permalink( $myaccount_page_id );
}
$cart_url = wc_get_cart_url();
?>
<div class="main-navigation">
	<nav class="navbar navbar-expand-xl navbar-light">
		<div class="header-top-wrapper">
			<div class="left-menu-content">
				<div class="lg-breadcrumb">
					<?php echo do_shortcode( '[wpseo_breadcrumb]' ) ?>
				</div>
				<div class="menu-and-5yr">
					<div class="svg-5yr">
						<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
						     viewBox="0 0 23.01 15.44">
							<defs>
								<style>.cls-1 {
										font-size: 15px;
										font-family: OpenSans-Bold, Open Sans;
										font-weight: 700;
									}

									.cls-1, .cls-2, .cls-3 {
										fill: #231f20;
									}

									.cls-2 {
										font-size: 5.8px;
									}

									.cls-2, .cls-3 {
										font-family: OpenSans-Semibold, Open Sans;
										font-weight: 600;
									}

									.cls-3 {
										font-size: 16px;
										text-decoration: underline;
									}</style>
							</defs>
							<title>5YR</title>
							<text class="cls-1" transform="translate(-0.41 10.91)">5</text>
							<text class="cls-2" transform="translate(0.35 15.01) scale(0.7)">WARRANTY</text>
							<text class="cls-3" transform="translate(8.97 8.32) scale(0.7)">YR</text>
						</svg>
					</div>
					<?php echo wp_nav_menu( array( 'menu' => 'Subscribe Menu' ) ) ?>
				</div>

			</div>
			<button class="navbar-toggler hamburger hamburger--squeeze js-hamburger" type="button"
			        data-toggle="offcanvas">
				<div class="hamburger-box">
					<div class="hamburger-inner"></div>
				</div>
				<div class="hamburger__menu">MENU</div>
			</button>

			<a class="navbar-brand m-auto" href="/">
				<?php echo site_logo(); ?>
				<span><?php echo get_bloginfo( 'description' ) ?></span>
			</a>

			<div class="right-menu-content">
				<div class="right-menu-links">

					<?php if ( $myaccount_page_url ): ?>
						<a href="<?php echo $myaccount_page_url ?>">
							<i class="fas fa-user-circle"></i>
						</a>
					<?php endif; ?>
					<a href="<?php echo $cart_url ?>">
						<i class="fas fa-shopping-cart"></i>
					</a>
					<?php echo do_shortcode( '[wcpbc_country_selector other_countries_text=International]' ) ?>
				</div>
			</div>
		</div>
		<!-- Main Menu  -->
		<div class="navbar-collapse offcanvas-collapse" id="main-navbar">
			<?php echo get_search_form() ?>
			<?php lg_print_nav_menu(); ?>

			<!--<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="#">Link</a>
				</li>

				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="dropdown03" data-toggle="dropdown"
					   aria-haspopup="true" aria-expanded="false">Dropdown</a>
					<div class="dropdown-menu" aria-labelledby="dropdown03">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</li>
			</ul>-->
			<?php

			?>
		</div>
	</nav>
</div>
