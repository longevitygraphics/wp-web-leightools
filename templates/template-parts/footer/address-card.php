
<!-- Default Address Stuff -->

<address class="address-card">
	
	<?php if (do_shortcode('[lg-address1]')) {echo do_shortcode('[lg-address1]') . '<br>'; } ?>
	<?php if (do_shortcode('[lg-address2]')) {echo do_shortcode('[lg-address2]') . '<br>'; } ?>
	<?php if (do_shortcode('[lg-city]')) { echo do_shortcode('[lg-city]'); } ?>, 
	<?php if (do_shortcode('[lg-province]')) { echo do_shortcode('[lg-province]'); }?> 
	<?php if (do_shortcode('[lg-city]') || do_shortcode('[lg-province]')) {echo '<br>'; } ?>
	<?php if ( do_shortcode('[lg-postcode]') ) { echo do_shortcode('[lg-postcode]') . '<br>'; } ?>

	<?php if (do_shortcode('[lg-contact-person]') || do_shortcode('[lg-phone-main]') || do_shortcode('[lg-phone-alt]') ||  do_shortcode('[lg-email]') ) {
		echo "<br>";
	} ?>

	<?php if ( do_shortcode('[lg-contact-person]') ) { echo do_shortcode('[lg-contact-person]') . '<br>'; } ?>
	
	<?php if (do_shortcode('[lg-phone-main]')) : ?>
		<abbr title="Phone">P: <?php echo format_phone( do_shortcode('[lg-phone-main]'))  . '<br>'; ?> </abbr>
	<?php endif; ?>

	<?php if (do_shortcode('[lg-phone-alt]')) : ?>
		<abbr title="Toll Free Phone">P: <?php echo format_phone( do_shortcode('[lg-phone-alt]'))  . '<br>'; ?> </abbr>
	<?php endif; ?>

	<?php if (do_shortcode('[lg-phone-alt]')) : ?>
		<abbr title="Fax">F: <?php echo format_phone( do_shortcode('[lg-fax]'))  . '<br>'; ?> </abbr>
	<?php endif; ?>

	<?php if (do_shortcode('[lg-email]')) : ?>
		<abbr title="Email">F: <?php echo do_shortcode('[lg-email]') . '<br>'; ?> </abbr>
	<?php endif; ?>

</address>