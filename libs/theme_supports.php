<?php
function add_child_template_support() {
	/*add theme colors*/
	add_theme_support( 'editor-color-palette', array(
		array(
			'name'  => __( 'Primary', 'wp-theme-parent' ),
			'slug'  => 'primary',
			'color' => '#424242'
		),
		array(
			'name'  => __( 'Secondary', 'wp-theme-parent' ),
			'slug'  => 'secondary',
			'color' => '#202020'
		),
		array(
			'name'  => __( 'White', 'wp-theme-parent' ),
			'slug'  => 'white',
			'color' => '#ffffff'
		),
		array(
			'name'  => __( 'Dark', 'wp-theme-parent' ),
			'slug'  => 'dark',
			'color' => '#151515'
		),
		array(
			'name'  => __( 'BG', 'wp-theme-parent' ),
			'slug'  => 'bg',
			'color' => '#ecebe8'
		)
	) );

	//add support for editor styles
	add_theme_support( "editor-styles" );

	// Footer Delta ~ Fourth Item
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Delta', '_s' ),
			'id'            => 'footer-delta',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

add_action( 'after_setup_theme', 'add_child_template_support', 20 );
