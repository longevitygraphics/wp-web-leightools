<?php

/**
 * https://wordpress.stackexchange.com/questions/170033/convert-output-of-nav-menu-items-into-a-tree-like-multidimensional-array
 * @param array $elements
 * @param int $parentId
 *
 * @return array
 */
function buildTree( array &$elements, $parentId = 0 ) {
	$branch = array();
	foreach ( $elements as &$element ) {
		if ( $element->menu_item_parent == $parentId ) {
			$children = buildTree( $elements, $element->ID );
			if ( $children ) {
				$element->menu_children = $children;
			}

			$branch[ $element->ID ] = $element;
			unset( $element );
		}
	}

	return $branch;
}

function lg_nav_menu_tree( $menu_id ) {
	$items = wp_get_nav_menu_items( $menu_id );

	return $items ? buildTree( $items, 0 ) : null;
}

function pr( $arr ) {
	echo "<pre>";
	print_r( $arr );
	echo "</pre>";
}

function lg_print_nav_menu() {
	$menu_array = lg_nav_menu_tree( 2 );
	//pr( $menu_array );


	$menu = '<ul class="navbar-nav mr-auto">';
	foreach ( $menu_array as $menu_item ) {
		$has_children               = isset( $menu_item->menu_children );
		$dropdown_class             = ( $has_children ) ? 'dropdown ' : '';
		$dropdown_class             .= ( ! empty( $menu_item->classes ) && is_array( $menu_item->classes ) ) ? implode( ' ', $menu_item->classes ) : '';
		$dropdown_toggle_class      = ( $has_children ) ? 'dropdown-toggle' : '';
		$dropdown_toggle_attributes = ( $has_children ) ? 'data-toggle="dropdown" aria-expanded="false"' : '';
		$dropdown_menu              = ( $has_children ) ? get_dropdown_menu( $menu_item ) : '';
		$menu                       .= '
			<li class="nav-item ' . $dropdown_class . '">
				<a 
					id="menu-item-' . $menu_item->ID . '"
					class="nav-link ' . $dropdown_toggle_class . '" 					
					href="' . $menu_item->url . '" 
					' . $dropdown_toggle_attributes . '
				>' . $menu_item->title . '</a>
				' . $dropdown_menu . '
			</li>';

	}

	$menu .= '</ul>';

	echo $menu;
}

//LEVEL 2
function get_dropdown_menu( $menu_item ) {
	$menu = '<ul class="dropdown-menu" aria-labelledby="menu-item-' . $menu_item->ID . '">';
	foreach ( $menu_item->menu_children as $item ) {
		$has_children           = isset( $item->menu_children );
		$dropdown_submenu_class = $has_children ? 'dropdown-submenu ' : '';
		$dropdown_submenu_class .= ( ! empty( $item->classes ) && is_array( $item->classes ) ) ? implode( ' ', $item->classes ) : '';

		$dropdown_submenu = ( $has_children ) ? get_dropdown_submenu( $item ) : '';
		$menu             .= '
				<li class="' . $dropdown_submenu_class . '">'
		                     . get_menu_link( $item )
		                     . $dropdown_submenu
		                     . '</li>';
	}
	$menu .= '</ul>';


	return $menu;
}

//level 3
function get_dropdown_submenu( $menu_item ) {
	$menu = '';
	$menu = '<ul class="dropdown-menu" aria-labelledby="menu-item-' . $menu_item->ID . '">';
	foreach ( $menu_item->menu_children as $item ) {
		$has_children           = isset( $item->menu_children );
		$dropdown_submenu_class = $has_children ? 'dropdown-submenu ' : '';
		$dropdown_submenu_class .= ( ! empty( $item->classes ) && is_array( $item->classes ) ) ? implode( ' ', $item->classes ) : '';
		$dropdown_toggle_class  = ( $has_children ) ? 'dropdown-toggle' : '';
		$menu                   .= '
				<li class="' . $dropdown_submenu_class . '">
					<a 
						class="dropdown-item ' . $dropdown_toggle_class . '" 
						href="' . $item->url . '"
					>' . $item->title . '</a>
					
				</li>';
	}
	$menu .= '</ul>';

	return $menu;
}


function get_menu_link( $item ) {
	$has_children                = isset( $item->menu_children );
	$has_link                    = ( $item->url === '#' ) ? false : true;
	$dropdown_toggle_class       = ( $has_children && ! $has_link ) ? 'dropdown-toggle' : '';
	$dropdown_toggle_attributes  = ( $has_children && ! $has_link ) ? 'data-toggle="dropdown" aria-expanded="false"' : '';
	$menu_item_id                = ( $has_children && ! $has_link ) ? 'id="menu-item-' . $item->ID . '"' : '';
	$dropdown_toggle_class1      = ( $has_children && $has_link ) ? 'dropdown-toggle' : '';
	$dropdown_toggle_attributes1 = ( $has_children && $has_link ) ? 'data-toggle="dropdown" aria-expanded="false"' : '';
	$menu_item_id1               = ( $has_children && $has_link ) ? 'id="menu-item-' . $item->ID . '"' : '';


	//if item has link, we need to add toggle functionality only to the plus icon
	return
		'<a 			
			class="dropdown-item ' . $dropdown_toggle_class . '" 
			href="' . $item->url . '"
			' . $dropdown_toggle_attributes . '
			' . $menu_item_id . '
		>'

		. $item->title
		. '</a>'
		. '<span 
			class="dropdown-toggler"
			class="' . $dropdown_toggle_class1 . '"
			' . $dropdown_toggle_attributes1 . '
			' . $menu_item_id1 . '
			> 
		</span>';

}




