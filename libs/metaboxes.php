<?php

/**
 * enqueue metabox js to admin panel
 */
function load_custom_wp_admin_style() {
	wp_enqueue_script( 'my_custom_script', get_stylesheet_directory_uri() . '/admin_assets/js/scripts.js' );
}

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

/**
 * add metabox
 */
function product_banner_image_add_metabox() {
	add_meta_box( 'product_banner_image_div', __( 'Product Banner Image', 'text-domain' ), 'product_banner_image_metabox', 'product', 'side', 'high' );
}

add_action( 'add_meta_boxes', 'product_banner_image_add_metabox' );


/**
 * metabo callback
 *
 * @param $post
 */
function product_banner_image_metabox( $post ) {
	global $content_width, $_wp_additional_image_sizes;
	$image_id          = get_post_meta( $post->ID, '_listing_image_id', true );
	$old_content_width = $content_width;
	$content_width     = 254;
	if ( $image_id && get_post( $image_id ) ) {
		if ( ! isset( $_wp_additional_image_sizes['post-thumbnail'] ) ) {
			$thumbnail_html = wp_get_attachment_image( $image_id, array( $content_width, $content_width ) );
		} else {
			$thumbnail_html = wp_get_attachment_image( $image_id, 'post-thumbnail' );
		}
		if ( ! empty( $thumbnail_html ) ) {
			$content = $thumbnail_html;
			$content .= '<p class="hide-if-no-js"><a href="javascript:;" id="remove_listing_image_button" >' . esc_html__( 'Remove listing image', 'text-domain' ) . '</a></p>';
			$content .= '<input type="hidden" id="upload_listing_image" name="_listing_cover_image" value="' . esc_attr( $image_id ) . '" />';
		}
		$content_width = $old_content_width;
	} else {
		$content = '<img src="" style="width:' . esc_attr( $content_width ) . 'px;height:auto;border:0;display:none;" />';
		$content .= '<p class="hide-if-no-js"><a title="' . esc_attr__( 'Set listing image', 'text-domain' ) . '" href="javascript:;" id="upload_listing_image_button" id="set-listing-image" data-uploader_title="' . esc_attr__( 'Choose an image', 'text-domain' ) . '" data-uploader_button_text="' . esc_attr__( 'Set listing image', 'text-domain' ) . '">' . esc_html__( 'Set listing image', 'text-domain' ) . '</a></p>';
		$content .= '<input type="hidden" id="upload_listing_image" name="_listing_cover_image" value="" />';
	}
	echo $content;
}


/**
 * metabox save
 *
 * @param $post_id
 */
function product_banner_image_save( $post_id ) {
	if ( isset( $_POST['_listing_cover_image'] ) ) {
		$image_id = (int) $_POST['_listing_cover_image'];
		update_post_meta( $post_id, '_listing_image_id', $image_id );
	}
}

add_action( 'save_post', 'product_banner_image_save', 10, 1 );
