<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	
	<?php do_action('wp_body_end'); ?>
	<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>

	<footer id="site-footer">
		
		<div>
			<div id="site-footer-main" class="clearfix px-3 px-md-5 py-4 justify-content-center align-items-start flex-wrap">
				<div class="d-flex flex-wrap justify-content-between">
                    <div class="footer-quick-links d-md-flex">
					    <div class="site-footer-alpha footer-menu">
                            <h2 class='toggle-button'>Products <i class="fas fa-plus d-md-none"></i><i class="fas fa-minus d-md-none"></i></h2>
                            <?php dynamic_sidebar('footer-alpha'); ?>
                        </div>
					    <div class="site-footer-bravo footer-menu">
                            <h2 class='toggle-button'>Product Support <i class="fas fa-plus d-md-none"></i><i class="fas fa-minus d-md-none"></i></h2>
                            <?php dynamic_sidebar('footer-bravo'); ?>
                        </div>
					    <div class="site-footer-charlie footer-menu">
                            <h2 class='toggle-button'>Customer Support <i class="fas fa-plus d-md-none"></i><i class="fas fa-minus d-md-none"></i></h2>
                            <?php dynamic_sidebar('footer-charlie'); ?>
                        </div>
                    </div>
                    <div class="footer-branding">
                        <div class="site-footer-charlie text-center d-none d-md-block"><?php dynamic_sidebar('footer-delta'); ?></div>
                    </div>
				</div>
            </div>

            <div class="footer-legal py-2 px-3 px-md-5">
                <?php
					$footer_legal_menu = array(
						'theme_location' => 'bottom-nav',
					);
					wp_nav_menu( $footer_legal_menu );
				?>
            </div>

        </div>
        <?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
				<div id="footer-copyright" class="py-2 px-3 px-md-5">
					<div class="d-flex justify-content-center justify-content-between align-items-center flex-wrap">
						<div class="site-info"><span>&copy; Copyright <?php echo date("Y") ?> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Leigh Industries Ltd.</a> All rights reserved.</span></div>
						<div class="site-comodo"><img src="<?php echo get_stylesheet_directory_uri()?>/assets/dist/images/comodo_secure.png" alt="Comodo Secure"></div>
					</div>
				</div>
			<?php endif; ?>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action('document_end'); ?>
